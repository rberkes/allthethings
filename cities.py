import os
from random import randint
from lib.city import City
ROOTDIR=(os.path.dirname(os.path.realpath(__file__)))
fp=open(ROOTDIR+'/dicts/city.part.1','r')
sp=open(ROOTDIR+'/dicts/city.part.2','r')
POPMAX=500
flist=[]
slist=[]
for f in fp:
  f = f.strip().split()
  flist.append(f[0])

for s in sp:
  s = s.strip().split()
  slist.append(s[0])

CITYLIST=[]

for a in range(10):
  b = flist.pop(randint(0,len(flist)-1))
  c = slist.pop(randint(0,len(slist)-1))
  city=b+c
  pop=randint(0,POPMAX)
  cl = City(city,pop)
  CITYLIST.append(cl)
#  print(city)
count=1
for city in CITYLIST:
  print(str(count)+') '+str(city.population)+' '+city.name)
  count+=1
