import os
from random import randint
from lib.creature import Creature
ROOTDIR=(os.path.dirname(os.path.realpath(__file__)))
fp=open(ROOTDIR+'/dicts/name.part.1','r')
sp=open(ROOTDIR+'/dicts/name.part.2','r')
POPMAX=500
flist=[]
slist=[]
for f in fp:
  f = f.strip().split()
  flist.append(f[0])

for s in sp:
  s = s.strip().split()
  slist.append(s[0])

CREATURELIST=[]

for a in range(10):
  b = flist.pop(randint(0,len(flist)-1))
  c = slist.pop(randint(0,len(slist)-1))
  name=b+c
  pop=randint(0,POPMAX)
  cl = Creature(name,pop)
  CREATURELIST.append(cl)
#  print(name)
count=1
for creature in CREATURELIST:
  print(str(count)+') '+str(creature.population)+' '+creature.name+'s')
  count+=1
