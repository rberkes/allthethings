from django.shortcuts import render
from django.http import HttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render 
import lib.bb as bb
def index(request):
    return HttpResponse("This is a sample index for bbapp. You're at it now.")

# Create your views here.

def list(request):
    PLAYERLIST=[]
    try:                                                                                                         
        PLAYERLIST = bb.load_playerlist()
    except:
        print("no players!")                                   
    paginator = Paginator(PLAYERLIST,25)
    page=request.GET.get('page')
    try:
        players = paginator.page(page)
    except PageNotAnInteger:
        players = paginator.page(1)
    except EmptyPage:
        players = paginator.page(paginator.num_pages)
    return render(request, 'list.html', {'players':players}) 

def team(request):
    TEAMLIST=[]
    try:                                                                                                         
        TEAMLIST = bb.load_citylist()
    except:
        print("no players!")                                   
    paginator = Paginator(TEAMLIST,25)
    page=request.GET.get('page')
    try:
        teams = paginator.page(page)
    except PageNotAnInteger:
        teams = paginator.page(1)
    except EmptyPage:
        teams = paginator.page(paginator.num_pages)
    return render(request, 'teams.html', {'teams':teams}) 
