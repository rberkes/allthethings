import os
from random import randint
import shelve
from .bb_city import BBCity
from .bb_player import BBPlayer

NUMTEAMS = 8
ROOTDIR = (os.path.dirname(os.path.realpath(__file__)))
NUMPLAYERS = 8*30
RANDMAX = 100

def load_citylist():
    LIST = []
    with shelve.open('shelf-baseball','r') as shelf:
        LIST = shelf['teams']
    return LIST

def load_playerlist():
    LIST = []
    with shelve.open('shelf-baseball','r') as shelf:
        LIST = shelf['players']
    return LIST

def save_stuff(CITYLIST,PLAYERLIST):
    with shelve.open('shelf-baseball','c') as shelf:
      shelf['teams'] = CITYLIST
      shelf['players'] = PLAYERLIST
    return

def gen_players():
    FN = open(ROOTDIR+'/dicts/name.part.1','r')
    LN = open(ROOTDIR+'/dicts/name.part.2','r')
    plist = []
    flist = []
    llist = []
    for f in FN:
        f = f.strip().split()
        flist.append(f[0])

    for l in LN:
        l = l.strip().split()
        llist.append(l[0])

    poslist = ['P'] * 12 + ['C'] * 2 + ['1B']*2 +['2B']*2 +['3B']*2+['SS']*2+['LF']*2+['CF']*2+['RF']*2

    for count in range(NUMPLAYERS):
        fn = flist[randint(0,len(flist)-1)]
        ln = llist[randint(0,len(llist)-1)]
        name = fn + ' ' + ln
        position = poslist[randint(0,len(poslist)-1)]
        contact = randint(0,RANDMAX)
        power = randint(0,RANDMAX)
        speed = randint(0,RANDMAX)
        field = randint(0,RANDMAX)
        arm = randint(0,RANDMAX)
        pit_strength = randint(0,RANDMAX)
        control = randint(0,RANDMAX)
        groundballs = randint(0,RANDMAX)
        strikeouts = randint(0,RANDMAX)
        stamina = randint(0,RANDMAX)
        plist.append
        p = BBPlayer(name,count,fn,ln,position,contact,power,speed,field,arm,pit_strength,control,groundballs,strikeouts,stamina)
        plist.append(p)
    return plist

def gen_city_names():
    FP = open(ROOTDIR+'/dicts/city.part.1','r')
    SP = open(ROOTDIR+'/dicts/city.part.2','r')
    flist = []
    slist = []
    for f in FP:
      f = f.strip().split()
      flist.append(f[0])

    for s in SP:
      s = s.strip().split()
      slist.append(s[0])

    FP=open(ROOTDIR+'/dicts/animal.part.1','r')
    SP=open(ROOTDIR+'/dicts/animal.part.2','r')
    n1list=[]
    n2list=[]
    for f in FP:
      f = f.strip().split()
      n1list.append(f[0])

    for s in SP:
      s = s.strip().split()
      n2list.append(s[0])
    LL = []
    for a in range(NUMTEAMS):
      b = flist.pop(randint(0,len(flist)-1))
      c = slist.pop(randint(0,len(slist)-1))
      n1 = n1list.pop(randint(0,len(n1list)-1))
      n2 = n2list.pop(randint(0,len(n2list)-1))
      city=b+' '+c
      nick=n1+n2
      cl = BBCity(city,nick,a)
      LL.append(cl)
    return LL

