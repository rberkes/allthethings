from .thing import thing
class BBPlayer(thing):
   def __init__(self,name,_id,firstname,lastname,position,contact,power,speed,field,arm,
                pit_strength,control,groundballs,strikeouts,stamina,teamid=-1):
       super().__init__(name,_id)
       self.position = position
       self.firstname = firstname
       self.lastname = lastname
       self.teamid = teamid
       self.contact = contact
       self.power = power
       self.speed = speed
       self.field = field
       self.arm = arm
       self.pit_strength = pit_strength
       self.control = control
       self.groundballs = groundballs
       self.strikeouts = strikeouts
       self.stamina = stamina
  
   def __print__(self):
       print(str(self._id)+'.) '+self.firstname+' '+self.lastname+'\tPos: '+self.position+
        '\tCon ' + str(self.contact)+'\tPow '+str(self.power)+'\tSpd '+str(self.speed)+
        '\tFld '+str(self.field)+'\tArm '+str(self.arm)+'\tPitStr '+str(self.pit_strength)+
        '\tCon '+str(self.control)+'\tGrd '+str(self.groundballs)+'\tKs '+str(self.strikeouts)+
        '\tSta '+str(self.stamina))   
