import os
import lib.bb as bb
from pimento import menu 
ROOTDIR = (os.path.dirname(os.path.realpath(__file__)))
CITYLIST = []
try:
    CITYLIST = bb.load_citylist()
except:
    print("Could not load data file, gen'ing from scratch...")
    CITYLIST = bb.gen_city_names()

def city_print(CITYLIST):
    for city in CITYLIST:
        city.__print__()
    return

PLAYERLIST = []

try:
    PLAYERLIST = bb.load_playerlist()
except:
    print("generating players...")
    PLAYERLIST=bb.gen_players()


def player_print(PLAYERLIST):
    for p in PLAYERLIST:
        p.__print__()
    return

result=''
while result != 'Save and exit':
    result = menu (
      [ 'Players', 'Cities', 'Save and exit',], 
      "Please enter a choice:", indexed=True
    )
    if result == 'Players':
        player_print(PLAYERLIST)
    elif result == 'Cities':
        city_print(CITYLIST)
    






bb.save_stuff(CITYLIST,PLAYERLIST)
